<?php get_header(); ?>
    <header class="relative flex flex-col justify-center items-center aspect-video md:-mt-28 xl:-mt-36 ">
        <!--after:content[''] after:absolute after:h-full after:w-full after:bg-dark/70 after:z-10-->
        <!--        <div class="absolute z-20 text-light text-center">-->
        <!--        </div>-->
        <video autoplay loop muted poster="<?php echo get_template_directory_uri() . '/img/cover.jpg'; ?>"
               class="absolute w-full h-full object-cover">
            <source src="<?php site_url() ?>/wp-content/uploads/2022/10/Drone-1.m4v" type="video/mp4">
        </video>
    </header>
<?php
get_template_part('template-parts/section', 'news');
get_template_part('template-parts/section', 'funfact');
get_template_part('template-parts/section', 'article');
?>
<?php get_footer(); ?>