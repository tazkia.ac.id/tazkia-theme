<?php get_header(); ?>
<?php
/**
 * data field
 */
$dataFields = [
    'biodata' => [
        ['key' => 'nik', 'label' => 'NIK'],
        ['key' => 'position', 'label' => 'Position'],
        ['key' => 'email', 'label' => 'Email'],
        ['key' => 'room', 'label' => 'Room'],
        ['key' => 'office_hours', 'label' => 'Office Hours'],
        ['key' => 'areas', 'label' => 'Areas']
    ],
    'resumes' => [
        [
            'key' => 'biography',
            'label' => 'Biography',
            'icon' => '<svg xmlns="http://www.w3.org/2000/svg" class="bi bi-back fill-primary dark:fill-secondary w-7 h-7 mr-2" viewBox="0 0 16 16">
            <path d="M0 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v2h2a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2v-2H2a2 2 0 0 1-2-2V2zm2-1a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H2z"/></svg>'],
        [
            'key' => 'academic_qualifications',
            'label' => 'Academic Qualifications',
            'icon' => '<svg xmlns="http://www.w3.org/2000/svg"
                                     class="bi bi-cursor-fill fill-primary dark:fill-secondary w-7 h-7 mr-2"
                                     viewBox="0 0 16 16">
                                    <path d="M8.211 2.047a.5.5 0 0 0-.422 0l-7.5 3.5a.5.5 0 0 0 .025.917l7.5 3a.5.5 0 0 0 .372 0L14 7.14V13a1 1 0 0 0-1 1v2h3v-2a1 1 0 0 0-1-1V6.739l.686-.275a.5.5 0 0 0 .025-.917l-7.5-3.5Z"/>
                                    <path d="M4.176 9.032a.5.5 0 0 0-.656.327l-.5 1.7a.5.5 0 0 0 .294.605l4.5 1.8a.5.5 0 0 0 .372 0l4.5-1.8a.5.5 0 0 0 .294-.605l-.5-1.7a.5.5 0 0 0-.656-.327L8 10.466 4.176 9.032Z"/>
                                </svg>'],
        [
            'key' => 'professional_experiences',
            'label' => 'Professional Experiences',
            'icon' => '<svg xmlns="http://www.w3.org/2000/svg"
                                     class="bi bi-cursor-fill fill-primary dark:fill-secondary w-7 h-7 mr-2"
                                     viewBox="0 0 16 16">
                                    <path d="M6.5 1A1.5 1.5 0 0 0 5 2.5V3H1.5A1.5 1.5 0 0 0 0 4.5v1.384l7.614 2.03a1.5 1.5 0 0 0 .772 0L16 5.884V4.5A1.5 1.5 0 0 0 14.5 3H11v-.5A1.5 1.5 0 0 0 9.5 1h-3zm0 1h3a.5.5 0 0 1 .5.5V3H6v-.5a.5.5 0 0 1 .5-.5z"/>
                                    <path d="M0 12.5A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5V6.85L8.129 8.947a.5.5 0 0 1-.258 0L0 6.85v5.65z"/>
                                    </svg>'],
        [
            'key' => 'community_engagement',
            'label' => 'Community Engagement',
            'icon' => '<svg xmlns="http://www.w3.org/2000/svg"
                                     class="bi bi-cursor-fill fill-primary dark:fill-secondary w-7 h-7 mr-2"
                                     viewBox="0 0 16 16">
                                    <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H4.414a1 1 0 0 0-.707.293L.854 15.146A.5.5 0 0 1 0 14.793V2zm7.194 2.766a1.688 1.688 0 0 0-.227-.272 1.467 1.467 0 0 0-.469-.324l-.008-.004A1.785 1.785 0 0 0 5.734 4C4.776 4 4 4.746 4 5.667c0 .92.776 1.666 1.734 1.666.343 0 .662-.095.931-.26-.137.389-.39.804-.81 1.22a.405.405 0 0 0 .011.59c.173.16.447.155.614-.01 1.334-1.329 1.37-2.758.941-3.706a2.461 2.461 0 0 0-.227-.4zM11 7.073c-.136.389-.39.804-.81 1.22a.405.405 0 0 0 .012.59c.172.16.446.155.613-.01 1.334-1.329 1.37-2.758.942-3.706a2.466 2.466 0 0 0-.228-.4 1.686 1.686 0 0 0-.227-.273 1.466 1.466 0 0 0-.469-.324l-.008-.004A1.785 1.785 0 0 0 10.07 4c-.957 0-1.734.746-1.734 1.667 0 .92.777 1.666 1.734 1.666.343 0 .662-.095.931-.26z"/>
                                    </svg>'],
        [
            'key' => 'journals',
            'label' => 'Published Journals',
            'icon' => '<svg xmlns="http://www.w3.org/2000/svg"
                                     class="bi bi-cursor-fill fill-primary dark:fill-secondary w-7 h-7 mr-2"
                                     viewBox="0 0 16 16">
                                    <path d="M5 0h8a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2 2 2 0 0 1-2 2H3a2 2 0 0 1-2-2h1a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1H1a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v9a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H5a1 1 0 0 0-1 1H3a2 2 0 0 1 2-2z"/>
                                    <path d="M1 6v-.5a.5.5 0 0 1 1 0V6h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V9h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 2.5v.5H.5a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1H2v-.5a.5.5 0 0 0-1 0z"/>
                                    </svg>'],
        [
            'key' => 'books',
            'label' => 'Published Books & Book Chapters',
            'icon' => '<svg xmlns="http://www.w3.org/2000/svg"
                                     class="bi bi-cursor-fill fill-primary dark:fill-secondary w-7 h-7 mr-2"
                                     viewBox="0 0 16 16">
                                    <path d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811V2.828zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492V2.687zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"/>
                                    </svg>'],
        [
            'key' => 'conference_papers',
            'label' => 'Conference Papers',
            'icon' => '<svg xmlns="http://www.w3.org/2000/svg"
                                     class="bi bi-cursor-fill fill-primary dark:fill-secondary w-7 h-7 mr-2"
                                     viewBox="0 0 16 16">
                                    <path d="M8.5 12v1.134a1 1 0 1 1-1 0V12h-5A1.5 1.5 0 0 1 1 10.5V3h14v7.5a1.5 1.5 0 0 1-1.5 1.5h-5Zm7-10a.5.5 0 0 0 0-1H.5a.5.5 0 0 0 0 1h15Z"/>
                                    </svg>'],
        [
            'key' => 'grants',
            'label' => 'Grants',
            'icon' => '<svg xmlns="http://www.w3.org/2000/svg"
                                     class="bi bi-cursor-fill fill-primary dark:fill-secondary w-7 h-7 mr-2"
                                     viewBox="0 0 16 16">
                                  <path d="M8 7.982C9.664 6.309 13.825 9.236 8 13 2.175 9.236 6.336 6.31 8 7.982Z"/>
                                  <path d="M3.75 0a1 1 0 0 0-.8.4L.1 4.2a.5.5 0 0 0-.1.3V15a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V4.5a.5.5 0 0 0-.1-.3L13.05.4a1 1 0 0 0-.8-.4h-8.5Zm0 1H7.5v3h-6l2.25-3ZM8.5 4V1h3.75l2.25 3h-6ZM15 5v10H1V5h14Z"/>
                        </svg>'],
        [
            'key' => 'awards',
            'label' => 'Awards',
            'icon' => '<svg xmlns="http://www.w3.org/2000/svg"
                                     class="bi bi-cursor-fill fill-primary dark:fill-secondary w-7 h-7 mr-2"
                                     viewBox="0 0 16 16">
                                    <path d="M9.669.864 8 0 6.331.864l-1.858.282-.842 1.68-1.337 1.32L2.6 6l-.306 1.854 1.337 1.32.842 1.68 1.858.282L8 12l1.669-.864 1.858-.282.842-1.68 1.337-1.32L13.4 6l.306-1.854-1.337-1.32-.842-1.68L9.669.864zm1.196 1.193.684 1.365 1.086 1.072L12.387 6l.248 1.506-1.086 1.072-.684 1.365-1.51.229L8 10.874l-1.355-.702-1.51-.229-.684-1.365-1.086-1.072L3.614 6l-.25-1.506 1.087-1.072.684-1.365 1.51-.229L8 1.126l1.356.702 1.509.229z"/>
                                    <path d="M4 11.794V16l4-1 4 1v-4.206l-2.018.306L8 13.126 6.018 12.1 4 11.794z"/>
                        </svg>'],
    ]
];
?>
<?php if (have_posts()) : ?>
    <?php
    while (have_posts()) :
        the_post();
        ?>
        <header class="bg-disable/10 dark:bg-disable/30 px-4">
            <div class="container max-w-screen-md mx-auto py-28 text-dark dark:text-light">
                <div class="flex flex-row flex-wrap">
                    <div class="basis-full md:basis-1/3 flex justify-center items-center">
                        <div class="aspect-square overflow-hidden p-3 bg-light rounded-lg relative shadow-xl shadow-disable/30 max-w-xs">
                            <img class="object-cover w-full h-full rounded"
                                 src="<?php if (has_post_thumbnail()) {
                                     the_post_thumbnail_url();
                                 } else {
                                     echo get_template_directory_uri() . '/img/postcover.svg';
                                 } ?>" alt="<?php the_title(); ?>">
                        </div>
                    </div>
                    <div class="basis-full md:basis-2/3 p-4 flex flex-col items-center md:items-start">
                        <h1 class="text-2xl mb-3 font-semibold"><?php echo the_title() ?></h1>
                        <table>
                            <?php
                            $biodataFields = $dataFields['biodata'];
                            foreach ($biodataFields as $biodata) :
                                if (get_post_meta(get_the_ID(), $biodata['key'], true)) :
                                    ?>
                                    <tr>
                                        <td><?php echo $biodata['label']; ?></td>
                                        <td class="px-2">:</td>
                                        <td><?php echo get_post_meta(get_the_ID(), $biodata['key'], true) ?></td>
                                    </tr>
                                <?php endif; endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </header>
        <div class="px-4">
            <div class="container max-w-screen-md mx-auto">
                <article id="post-<?php the_ID(); ?>" <?php post_class('my-10'); ?>>
                    <?php
                    $resumes = $dataFields['resumes'];
                    foreach ($resumes as $resume) :
                        if (get_post_meta(get_the_ID(), $resume['key'], true)) :
                            ?>
                            <h2 class="text-3xl text-center text-dark dark:text-light md:text-left font-semibold mb-3 flex flex-row flex-wrap items-center">
                                <?php echo $resume['icon'];
                                echo $resume['label']; ?></h2>
                            <div class="entry-content text-dark dark:text-light">
                                <?php
                                $content = get_post_meta(get_the_ID(), $resume['key'], true);
                                $content = apply_filters('the_content', $content);
                                echo $content;
                                ?>
                            </div>
                        <?php endif; endforeach; ?>
                </article>
            </div>
        </div>
    <?php endwhile; ?>

<?php endif; ?>
<?php
get_footer();
