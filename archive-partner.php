<?php get_header(); ?>
<header class="bg-disable/10 dark:bg-disable/30 px-4">
    <div class="container mx-auto py-28 text-dark dark:text-light">
        <p class="text-5xl font-bold font-DM-sans">Mitra Tazkia</p>
        <p class="text-2xl">Mita Nasional dan Internasional Institut Agama Islam Tazkia</p>
    </div>
</header>
<section class="px-4 my-20">
    <div class="container mx-auto">
        <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-4">
            <?php if (have_posts()) :
                while (have_posts()) :
                    the_post();
                    ?>
                    <a href="<?php echo get_permalink(); ?>">
                        <div class="bg-light dark:bg-disable/25 hover:bg-disable/5 dark:hover:bg-disable/50 dark:text-light shadow-xl shadow-disable/10 p-4 rounded-lg">
                            <div class="aspect-video overflow-hidden rounded-lg relative">
                                <img class="h-full w-full object-cover rounded"
                                     src="<?php if (has_post_thumbnail()) {
                                         the_post_thumbnail_url();
                                     } else {
                                         echo get_template_directory_uri() . '/img/postcover.svg';
                                     } ?>" alt="<?php the_title(); ?>">
                            </div>
                        </div>
                    </a>
                <?php
                endwhile;
            endif; ?>
        </div>
        <?php get_template_part('template-parts/pagination'); ?>
    </div>
</section>
<?php get_footer(); ?>
