<?php get_header(); ?>

    <div class="container max-w-screen-lg mb-20 mx-auto px-4">

        <?php if (have_posts()) : ?>

            <?php
            while (have_posts()) :
                the_post();
                ?>
                <div class="flex flex-row flex-wrap mt-12 sm:flex-row-reverse">
                    <div class="basis-full sm:basis-8/12 text-dark dark:text-light">
                        <?php the_title(sprintf('<h1 class="text-2xl md:text-3xl lg:text-4xl font-DM-sans font-extrabold leading-tight mb-5"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h1>'); ?>
                        <div class="entry-content">

                            <?php the_content(); ?>

                        </div>
                    </div>
                    <div class="basis-full sm:basis-4/12">
                        <img class="w-full pr-4" src="<?php if (has_post_thumbnail()) {
                            the_post_thumbnail_url();
                        } else {
                            echo get_template_directory_uri() . '/img/rektor.png';
                        } ?>" alt="<?php the_title(); ?>">
                    </div>
                </div>

            <?php endwhile; ?>

        <?php endif; ?>

    </div>
<?php
get_footer();
