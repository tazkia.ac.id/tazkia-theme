<?php get_header(); ?>
<header class="bg-disable/10 dark:bg-disable/30 px-4">
    <div class="container mx-auto py-28 text-dark dark:text-light">
        <p class="text-5xl font-bold font-DM-sans mb-4"><?php echo ucfirst(get_the_archive_title()); ?></p>
        <p class="text-2xl"><?php echo get_the_archive_description() ?></p>
    </div>
</header>
<section class="px-4 my-20">
    <div class="container mx-auto">
        <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-4">
            <?php if (have_posts()) :
                while (have_posts()) :
                    the_post();
                    get_template_part('template-parts/content', 'list');
                endwhile;
            endif; ?>
        </div>
        <?php get_template_part('template-parts/pagination'); ?>
    </div>
</section>
<?php get_footer(); ?>
