<?php get_header(); ?>
<header class="bg-disable/10 dark:bg-disable/30 px-4">
    <div class="container lg:max-w-screen-lg mx-auto py-28 text-dark dark:text-light">
        <p class="text-5xl font-bold font-DM-sans">Rector Insight</p>
        <p class="text-2xl">Insight Rektor Institut Agama Islam Tazkia</p>
    </div>
</header>
<section class="px-4 my-20">
    <div class="container lg:max-w-screen-lg mx-auto">
        <div class="flex flex-row flex-wrap mb-4">
            <div class="basis-full sm:basis-4/12">
                <img class="mb-4 w-full" src="<?php echo get_template_directory_uri() . '/img/rektor.png'; ?>"
                     alt="rektor">
            </div>
            <div class="basis-full sm:basis-8/12 sm:pl-5">
                <?php if (have_posts()) :
                    while (have_posts()) :
                        the_post();
                        ?>
                        <div class="p-4 text-dark dark:text-light">
                            <a href="<?php the_permalink(); ?>"><?php the_title('<h2 class="text-4xl font-semibold mb-4">', '</h2>') ?></a>
                            <?php the_excerpt(); ?>
                            <a class="flex items-center mt-4 text-dark dark:text-light hover:text-primary dark:hover:text-secondary"
                               href="<?php the_permalink(); ?>">
                                <svg class="w-4 h-4 mr-1" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M14 5l7 7m0 0l-7 7m7-7H3"></path>
                                </svg>
                                <span>Selengkapnya</span>
                            </a>
                        </div>
                    <?php
                    endwhile;
                endif; ?>
            </div>
        </div>
    </div>
    <?php get_template_part('template-parts/pagination'); ?>
</section>
<?php get_footer(); ?>
