<?php

/**
 * Theme setup.
 */
function tazkia_theme_setup()
{
    add_theme_support('title-tag');

    register_nav_menus(
        array(
            'primary' => __('Primary Menu', 'tailpress'),
        )
    );

    add_theme_support(
        'html5',
        array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        )
    );

    add_theme_support('custom-logo');
    add_theme_support('post-thumbnails');

    add_theme_support('align-wide');
    add_theme_support('wp-block-styles');

    add_theme_support('editor-styles');
    add_editor_style('css/editor-style.css');
}

add_action('after_setup_theme', 'tazkia_theme_setup');

/**
 * Enqueue theme assets.
 */
function tazkia_theme_enqueue_scripts()
{
    $theme = wp_get_theme();
    if (is_front_page()) {
        wp_enqueue_style('swiper', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css', array(), '');
        wp_enqueue_script('swiperjs', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js', array(), $theme->get('Version'), true);
    }
    wp_enqueue_style('appcss', tazkia_theme_asset('css/app.css'), array(), $theme->get('Version'));
    wp_enqueue_script('appjs', tazkia_theme_asset('js/app.js'), array(), $theme->get('Version'), true);

}

add_action('wp_enqueue_scripts', 'tazkia_theme_enqueue_scripts');

/**
 * Get asset path.
 *
 * @param string $path Path to asset.
 *
 * @return string
 */
function tazkia_theme_asset($path)
{
    if (wp_get_environment_type() === 'production') {
        return get_stylesheet_directory_uri() . '/' . $path;
    }

    return add_query_arg('time', time(), get_stylesheet_directory_uri() . '/' . $path);
}


//filter archive title prefix
function my_theme_archive_title($title)
{
    if (is_category()) {
        $title = single_cat_title('', false);
    } elseif (is_tag()) {
        $title = single_tag_title('', false);
    } elseif (is_author()) {
        $title = get_the_author();
    } elseif (is_post_type_archive()) {
        $title = post_type_archive_title('', false);
    } elseif (is_tax()) {
        $title = single_term_title('', false);
    }

    return $title;
}

add_filter('get_the_archive_title', 'my_theme_archive_title');

//register custom post types
include get_template_directory() . '/inc/article-post-type.php';
include get_template_directory() . '/inc/rector-insight-post-type.php';
include get_template_directory() . '/inc/partner-post-type.php';
include get_template_directory() . '/inc/lecturer-post-type.php';
include get_template_directory() . '/inc/achievement-post-type.php';
include get_template_directory() . '/inc/facilities-post-type.php';
