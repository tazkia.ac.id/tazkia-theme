<?php
//labels for article post type
$labels = array(
    'name' => esc_html__('Articles'),
    'singular_name' => esc_html__('Article'),
    'add_new' => esc_html__('Add New'),
    'add_new_item' => esc_html__('Add New Article'),
    'edit_item' => esc_html__('Edit Article'),
    'new_item' => esc_html__('New Article'),
    'all_items' => esc_html__('All Article'),
    'view_item' => esc_html__('View Article'),
    'search_items' => esc_html__('Search Article'),
    'not_found' => esc_html__('No Teacher found'),
    'not_found_in_trash' => esc_html__('No Teacher found in Trash'),
    'menu_name' => esc_html__('Article')
);
//arguments for article post type
$args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => 5,
    'menu_icon' => 'dashicons-text-page',
    'show_in_rest' => true,
    'supports' => array('title', 'excerpt', 'editor', 'thumbnail', 'revisions', 'author'),
    'taxonomies' => array('category'),
    'rewrite' => array('slug' => 'article')
);
//register article post type
register_post_type('article', $args);

//label for article taxonomies category

$labels = array(
    'name' => 'Category',
    'singular_name' => 'Category',
    'all_items' => 'All Categories',
    'edit_item' => 'Edit Category',
    'update_item' => 'Update Category',
    'add_new_item' => 'Add New Category',
    'new_item_name' => 'New category Name'
);
//register article taxonomy categories
register_taxonomy('article_categories', 'article', array(
    'hierarchical' => true,
    'show_ui' => true,
    'labels' => $labels,
    'show_in_rest' => true,
    'rewrite' => array('slug' => 'article-category')
));