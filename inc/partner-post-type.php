<?php
//labels for partner post type
$labels = array(
    'name' => esc_html__('Partner'),
    'singular_name' => esc_html__('Partner'),
    'add_new' => esc_html__('Add New'),
    'add_new_item' => esc_html__('Add New Partner'),
    'edit_item' => esc_html__('Edit Partner'),
    'new_item' => esc_html__('New Partner'),
    'all_items' => esc_html__('All Partner'),
    'view_item' => esc_html__('View Partner'),
    'search_items' => esc_html__('Search Partner'),
    'not_found' => esc_html__('No Teacher found'),
    'not_found_in_trash' => esc_html__('No Teacher found in Trash'),
    'menu_name' => esc_html__('Partner')
);
//arguments for partner post type
$args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => 9,
    'menu_icon' => 'dashicons-admin-site-alt3',
    'supports' => array('title', 'editor', 'thumbnail',),
    'taxonomies' => array('category'),
    'rewrite' => array('slug' => 'partner')
);
//register partner post type
register_post_type('partner', $args);

//label for partner taxonomies category

$labels = array(
    'name' => 'Category',
    'singular_name' => 'Category',
    'all_items' => 'All Categories',
    'edit_item' => 'Edit Category',
    'update_item' => 'Update Category',
    'add_new_item' => 'Add New Category',
    'new_item_name' => 'New category Name'
);
//register partner taxonomy categories
register_taxonomy('partner_categories', 'partner', array(
    'hierarchical' => true,
    'show_ui' => true,
    'labels' => $labels,
    'rewrite' => array('slug' => 'partner-category')
));

add_action('do_meta_boxes', 'my_cpt_move_meta_box');

function my_cpt_move_meta_box()
{
    remove_meta_box('postimagediv', 'partner', 'side');
    add_meta_box('postimagediv', __("Partner's Logo"), 'post_thumbnail_meta_box', 'partner', 'normal', 'high');
}