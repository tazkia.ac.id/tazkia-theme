<?php
//labels for achievement post type
$labels = array(
    'name' => esc_html__('Achievement'),
    'singular_name' => esc_html__('Achievement'),
    'add_new' => esc_html__('Add New'),
    'add_new_item' => esc_html__('Add New Achievement'),
    'edit_item' => esc_html__('Edit Achievement'),
    'new_item' => esc_html__('New Achievement'),
    'all_items' => esc_html__('All Achievement'),
    'view_item' => esc_html__('View Achievement'),
    'search_items' => esc_html__('Search Achievement'),
    'not_found' => esc_html__('No Achievement found'),
    'not_found_in_trash' => esc_html__('No Achievement found in Trash'),
    'menu_name' => esc_html__('Achievement')
);
//arguments for achievement post type
$args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => 8,
    'menu_icon' => 'dashicons-awards',
    'show_in_rest' => true,
    'supports' => array('title', 'excerpt', 'editor', 'thumbnail', 'revisions'),
    'rewrite' => array('slug' => 'achievement')
);
//register achievement post type
register_post_type('achievement', $args);
