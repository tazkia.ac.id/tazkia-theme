<?php
//labels for lecturer post type
$labels = array(
    'name' => esc_html__('Lecturer'),
    'singular_name' => esc_html__('Lecturer'),
    'add_new' => esc_html__('Add New'),
    'add_new_item' => esc_html__('Add New Lecturer'),
    'edit_item' => esc_html__('Edit Lecturer'),
    'new_item' => esc_html__('New Lecturer'),
    'all_items' => esc_html__('All Lecturer'),
    'view_item' => esc_html__('View Lecturer'),
    'search_items' => esc_html__('Search Lecturer'),
    'not_found' => esc_html__('No Teacher found'),
    'not_found_in_trash' => esc_html__('No Teacher found in Trash'),
    'menu_name' => esc_html__('Lecturer')
);
//arguments for lecturer post type
$args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => 7,
    'menu_icon' => 'dashicons-businessman',
    'supports' => array('title', 'thumbnail', 'author'),
    'taxonomies' => array('faculty'),
    'show_in_rest' => true,
    'rewrite' => array('slug' => 'lecturer')
);
//register lecturer post type
register_post_type('lecturer', $args);

//label for lecturer taxonomies category

$labels = array(
    'name' => 'Faculty',
    'singular_name' => 'Faculty',
    'all_items' => 'All Faculties',
    'edit_item' => 'Edit Faculty',
    'update_item' => 'Update Faculty',
    'add_new_item' => 'Add New Faculty',
    'new_item_name' => 'New Faculty Name'
);
//register lecture taxonomy faculties
register_taxonomy('lecturer_faculties', 'lecturer', array(
    'hierarchical' => true,
    'show_ui' => true,
    'show_in_rest' => true,
    'labels' => $labels,
    'rewrite' => array('slug' => 'lecturer-faculty')
));