<?php
//labels for achievement post type
$labels = array(
    'name' => esc_html__('Facilities'),
    'singular_name' => esc_html__('Facility'),
    'add_new' => esc_html__('Add New'),
    'add_new_item' => esc_html__('Add New Facility'),
    'edit_item' => esc_html__('Edit Facility'),
    'new_item' => esc_html__('New Facility'),
    'all_items' => esc_html__('All Facilities'),
    'view_item' => esc_html__('View Facility'),
    'search_items' => esc_html__('Search Facility'),
    'not_found' => esc_html__('No Facility found'),
    'not_found_in_trash' => esc_html__('No Facility found in Trash'),
    'menu_name' => esc_html__('Facilities')
);
//arguments for achievement post type
$args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => 8,
    'menu_icon' => 'dashicons-building',
    'show_in_rest' => true,
    'supports' => array('title', 'excerpt', 'editor', 'thumbnail', 'revisions'),
    'rewrite' => array('slug' => 'facility')
);
//register achievement post type
register_post_type('facilities', $args);
