<?php
//labels for rector insight post type
$labels = array(
    'name' => esc_html__('Rector Insight'),
    'singular_name' => esc_html__('Rector Insight'),
    'add_new' => esc_html__('Add New'),
    'add_new_item' => esc_html__('Add New Rector Insight'),
    'edit_item' => esc_html__('Edit Rector Insight'),
    'new_item' => esc_html__('New Rector Insight'),
    'all_items' => esc_html__('All Rector Insight'),
    'view_item' => esc_html__('View Rector Insight'),
    'search_items' => esc_html__('Search Rector Insight'),
    'not_found' => esc_html__('No Teacher found'),
    'not_found_in_trash' => esc_html__('No Teacher found in Trash'),
    'menu_name' => esc_html__('Rector Insight')
);
//arguments for rector insight post type
$args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => 6,
    'menu_icon' => 'dashicons-id-alt',
    'show_in_rest' => true,
    'supports' => array('title', 'excerpt', 'editor', 'thumbnail', 'revisions'),
    'taxonomies' => array('category'),
    'rewrite' => array('slug' => 'rector-insight')
);
//register rector insight post type
register_post_type('rector_insight', $args);

//label for rector insight taxonomies category

$labels = array(
    'name' => 'Category',
    'singular_name' => 'Category',
    'all_items' => 'All Categories',
    'edit_item' => 'Edit Category',
    'update_item' => 'Update Category',
    'add_new_item' => 'Add New Category',
    'new_item_name' => 'New category Name'
);
//register rector insight taxonomy categories
register_taxonomy('rector_insight_categories', 'rector_insight', array(
    'hierarchical' => true,
    'show_ui' => true,
    'labels' => $labels,
    'show_in_rest' => true,
    'rewrite' => array('slug' => 'rector-insight-category')
));