const theme = require('./theme.json');
const tailpress = require("@jeffreyvr/tailwindcss-tailpress");
const flowbite = require("flowbite/plugin");

module.exports = {
    content: [
        './*.php',
        './**/*.php',
        './resources/css/*.css',
        './resources/js/*.js',
        './safelist.txt',
        './node_modules/flowbite/**/*.js'
    ],
    theme: {
        extend: {
            colors: {
                'primary': '#023E88',
                'secondary': '#F47920',
                'danger': '#F42020',
                'success': '#027500',
                'warning': '#FCD62B',
                'disable': '#384555',
                'light': '#ECF0F6',
                'dark': '#00031F'
            },
            fontFamily: {
                'DM-sans': ['DM Sans', 'sans-serif']
            },
            fontSize: tailpress.fontSizeMapper(tailpress.theme('settings.typography.fontSizes', theme))
        },
        screens: {
            'xs': '480px',
            'sm': '600px',
            'md': '782px',
            'lg': tailpress.theme('settings.layout.contentSize', theme),
            'xl': tailpress.theme('settings.layout.wideSize', theme),
            '2xl': '1440px'
        }
    },
    plugins: [
        tailpress.tailwind,
        flowbite,
        require('@tailwindcss/line-clamp'),
    ]
};
