<?php get_header(); ?>
<header class="bg-disable/10 dark:bg-disable/30 px-4">
    <div class="container mx-auto py-28 text-dark dark:text-light">
        <p class="text-5xl font-bold font-DM-sans">Berita Terkini</p>
        <p class="text-2xl">Jangan Lewatkan Berita Menarik Seputar Institut Tazkia</p>
    </div>
</header>
<section class="px-4 my-20">
    <div class="container mx-auto">
        <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-4">
            <?php if (have_posts()) : ?>
                <?php
                while (have_posts()) :
                    the_post();
                    ?>
                    <div
                            class="bg-light dark:bg-disable/25 hover:bg-disable/5 dark:hover:bg-disable/50 dark:text-light shadow-xl shadow-disable/10 p-4 rounded-lg">
                        <div>
                            <div class="aspect-video overflow-hidden rounded-lg relative">
                                <div class="absolute w-full h-full bg-gradient-to-t from-primary to-light/0"></div>
                                <img class="h-full w-full object-cover"
                                     src="<?php if (has_post_thumbnail()) {
                                         the_post_thumbnail_url();
                                     } else {
                                         echo get_template_directory_uri() . '/img/postcover.svg';
                                     } ?>" alt="<?php the_title(); ?>">
                            </div>
                            <a href="<?php echo get_permalink(); ?>"
                               class="text-lg line-clamp-1 hover:line-clamp-none my-4"><?php the_title(); ?></a>
                        </div>
                        <div class="flex flex-row w-full items-center">
                            <img class="h-10 w-10 rounded-full mr-2"
                                 src="<?php echo get_avatar_url(get_the_author_meta('ID')); ?>" alt="user">
                            <div class="w-full">
                                <p class="text-md"><?php the_author(); ?></p>
                                <p class="text-sm"><?php echo get_the_date("F d Y"); ?></p>
                            </div>
                        </div>
                    </div>

                <?php endwhile; ?>

            <?php endif; ?>
        </div>
        <?php get_template_part('template-parts/pagination'); ?>
    </div>
</section>
<?php get_footer(); ?>
