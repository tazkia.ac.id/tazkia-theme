<article class="text-dark dark:text-light px-4 mb-20" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container mx-auto">
        <header class="entry-header mb-4">
            <div class="aspect-video overflow-hidden rounded-lg relative mb-10 mt-10 shadow-xl shadow-disable/30">
                <img class="object-cover w-full h-full"
                     src="<?php if (has_post_thumbnail()) {
                         the_post_thumbnail_url();
                     } else {
                         echo get_template_directory_uri() . '/img/postcover.svg';
                     } ?>" alt="<?php the_title(); ?>">
            </div>
            <?php the_title(sprintf('<h1 class="text-2xl lg:text-4xl font-DM-sans font-extrabold leading-tight mb-4"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h1>'); ?>

            <div class="flex flex-row w-full items-center">
                <img class="h-10 w-10 rounded-full mr-2"
                     src="<?php echo get_avatar_url(get_the_author_meta('ID')); ?>" alt="user">
                <div class="w-full">
                    <p class="text-md"><?php the_author(); ?></p>
                    <time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished"
                          class="text-sm text-dark/80 dark:text-light/80"><?php echo get_the_date("F d Y"); ?></time>
                </div>
            </div>
        </header>

        <div class="entry-content">

            <?php the_content(); ?>

            <?php
            wp_link_pages(
                array(
                    'before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'tailpress') . '</span>',
                    'after' => '</div>',
                    'link_before' => '<span>',
                    'link_after' => '</span>',
                    'pagelink' => '<span class="screen-reader-text">' . __('Page', 'tailpress') . ' </span>%',
                    'separator' => '<span class="screen-reader-text">, </span>',
                )
            );
            ?>
        </div>
    </div>
</article>