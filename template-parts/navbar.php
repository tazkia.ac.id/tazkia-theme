<nav id="navbar"
     class="bg-light px-4 py-1 dark:bg-dark sticky top-0 w-full z-50">
    <div class="container flex flex-wrap justify-between items-center mx-auto">
        <a href="<?php echo site_url(); ?>" class="flex items-center">
            <img src="<?php echo get_template_directory_uri() . '/img/logo.svg'; ?>" class="h-16 dark:hidden"
                 alt="Institut Tazkia Logo">
            <img src="<?php echo get_template_directory_uri() . '/img/logo-white.svg'; ?>"
                 class="h-16 hidden dark:block" alt="Institut Tazkia Logo">
        </a>
        <button data-collapse-toggle="navbar-menu" type="button"
                class="inline-flex items-center p-1 text-md text-disable rounded-md md:hidden hover:bg-light focus:outline-none focus:ring-2 focus:ring-disable"
                aria-controls="navbar-menu" aria-expanded="false">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                 stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"/>
            </svg>
        </button>
        <div class="hidden w-full md:flex md:flex-row-reverse items-center md:w-auto" id="navbar-menu">
            <div class="flex flex-row">
                <button class="hover:bg-disable/10 dark:hover:bg-disable/30 focus:ring-2 focus:ring-disable/10 mx-2 text-dark dark:text-light rounded-md p-2 block"
                        id="toggle-theme">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor" class="w-6 h-6">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M12 18v-5.25m0 0a6.01 6.01 0 001.5-.189m-1.5.189a6.01 6.01 0 01-1.5-.189m3.75 7.478a12.06 12.06 0 01-4.5 0m3.75 2.383a14.406 14.406 0 01-3 0M14.25 18v-.192c0-.983.658-1.823 1.508-2.316a7.5 7.5 0 10-7.517 0c.85.493 1.509 1.333 1.509 2.316V18"/>
                    </svg>
                </button>
                <a href="https://spmb.tazkia.ac.id"
                   class="bg-primary dark:bg-secondary text-light rounded-md py-2 px-4 font-DM-sans block">Daftar
                    Kuliah
                </a>
            </div>
            <ul class="flex flex-col md:flex-row  md:text-sm md:font-medium">
                <li>
                    <a class="block px-3 py-2 hover:bg-disable/10 md:hover:bg-light md:dark:hover:bg-dark text-dark dark:text-light md:hover:text-primary md:dark:hover:text-secondary rounded-md"
                       href="<?php echo site_url(); ?>">Beranda</a>
                </li>
                <li>
                    <a type="button" data-menu-toggle="about"
                       class="block px-3 py-2 hover:bg-disable/10 md:hover:bg-light md:dark:hover:bg-dark text-dark dark:text-light md:hover:text-primary md:dark:hover:text-secondary rounded-md">Tentang
                        Kami</a>
                    <div id="about"
                         class="bg-light dark:bg-dark text-dark dark:text-light rounded-md shadow-lg shadow-disable/5 dark:shadow-disable/10 w-full md:w-44 p-3 relative md:absolute md:top-20 hidden">
                        <a href="<?php echo site_url(); ?>/profile"
                           class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Profil</a>
                        <a href="<?php echo site_url(); ?>/visi-misi"
                           class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Visi
                            &
                            Misi</a>
                        <!--                        <a href="--><?php //echo site_url(); ?><!--/branding"-->
                        <!--                           class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Branding</a>-->
                        <!--                        <a href="--><?php //echo site_url(); ?><!--/struktur-organisasi"-->
                        <!--                           class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Struktur-->
                        <!--                            Organisasi</a>-->
                        <!--                        <a href="--><?php //echo site_url(); ?><!--/faq"-->
                        <!--                           class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">FAQ</a>-->
                    </div>
                </li>
                <li>
                    <a type="button" data-menu-toggle="student"
                       class="block px-3 py-2 hover:bg-disable/10 md:hover:bg-light md:dark:hover:bg-dark text-dark dark:text-light md:hover:text-primary md:dark:hover:text-secondary rounded-md">Mahasiswa</a>
                    <div id="student"
                         class="bg-light dark:bg-dark text-dark dark:text-light rounded-md shadow-lg shadow-disable/5 dark:shadow-disable/10 w-full md:w-auto p-3 relative md:absolute md:top-20 hidden">
                        <div class="flex flex-wrap">
                            <div class="w-full md:w-auto">
                                <div class="p-3">
                                    <p class="font-bold text-md">Calon Mahasiswa</p>
                                    <hr class="mt-2 text-disable/10">
                                </div>
                                <!--                                <a href="-->
                                <?php //echo site_url(); ?><!--/faculty"-->
                                <!--                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Fakultas</a>-->
                                <!--                                <a href="-->
                                <?php //echo site_url(); ?><!--/student-achievement"-->
                                <!--                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Mahasiswa-->
                                <!--                                    Berprestasi</a>-->
                                <a href="https://spmb.tazkia.ac.id"
                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Pendaftaran
                                    Mahasiswa</a>
                                <!--                                <a href="-->
                                <?php //echo site_url(); ?><!--/campus-life"-->
                                <!--                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">kehidupan-->
                                <!--                                    Kampus</a>-->
                                <!--                                <a href="-->
                                <?php //echo site_url(); ?><!--/program"-->
                                <!--                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Program</a>-->
                                <a href="<?php echo site_url(); ?>/facility"
                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Fasilitas</a>
                                <a href="<?php echo site_url(); ?>/achievement"
                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Prestasi</a>
                                <a href="https://beasiswa.tazkia.ac.id"
                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Beasiswa</a>
                            </div>
                            <div class="w-full md:w-auto">
                                <div class="p-3">
                                    <p class="font-bold text-md">Mahasiswa</p>
                                    <hr class="mt-2 text-disable/10">
                                </div>
                                <a href="https://smile.tazkia.ac.id"
                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Smile</a>
                                <!--                                <a href="-->
                                <?php //echo site_url(); ?><!--/student-service-center"-->
                                <!--                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Pusat-->
                                <!--                                    Pelayanan Mahasiswa</a>-->
                                <!--                                <a href="-->
                                <?php //echo site_url(); ?><!--/career-center"-->
                                <!--                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Pusat-->
                                <!--                                    Pengembangan Karir</a>-->
                                <a href="https://library.tazkia.ac.id"
                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Perpustakaan</a>
                                <a href="https://ib.tazkia.ac.id"
                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Inkubator
                                    Bisnis</a>
                                <a href="https://elearning.tazkia.ac.id/"
                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">E-learning</a>
                                <!--                                <a href="-->
                                <?php //echo site_url(); ?><!--/komdis"-->
                                <!--                                   class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Komite-->
                                <!--                                    Disiplin</a>-->
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a class="block px-3 py-2 hover:bg-disable/10 md:hover:bg-light md:dark:hover:bg-dark text-dark dark:text-light md:hover:text-primary md:dark:hover:text-secondary rounded-md"
                       href="https://alumni.tazkia.ac.id">Alumni</a>
                </li>
                <li>
                    <a type="button" data-menu-toggle="campus"
                       class="block px-3 py-2 hover:bg-disable/10 md:hover:bg-light md:dark:hover:bg-dark text-dark dark:text-light md:hover:text-primary md:dark:hover:text-secondary rounded-md">Kampus</a>
                    <div id="campus"
                         class="bg-light dark:bg-dark text-dark dark:text-light rounded-md shadow-lg shadow-disable/5 dark:shadow-disable/10 w-full md:w-44 p-3 relative md:absolute md:top-20 hidden">
                        <a href="<?php echo site_url(); ?>/rector-insight"
                           class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Rector Insight</a>
                        <!--                        <a href="--><?php //echo site_url(); ?><!--/research"-->
                        <!--                           class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Penelitian</a>-->
                        <a href="<?php echo site_url(); ?>/article"
                           class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Artikel</a>
                        <!--                        <a href="--><?php //echo site_url(); ?><!--/event"-->
                        <!--                           class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Acara</a>-->
                        <!--                        <a href="--><?php //echo site_url(); ?><!--/journal"-->
                        <!--                           class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Jurnal</a>-->
                        <a href="<?php echo site_url(); ?>/partner"
                           class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Mitra</a>
                        <a href="<?php echo site_url(); ?>/lecturer"
                           class="block p-3 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-md">Dosen</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>


