<div
        class="bg-light dark:bg-disable/25 hover:bg-disable/5 dark:hover:bg-disable/50 dark:text-light shadow-xl shadow-disable/10 p-4 rounded-lg">
    <div>
        <div class="aspect-video overflow-hidden rounded-lg relative">
            <img class="h-full w-full object-cover"
                 src="<?php if (has_post_thumbnail()) {
                     the_post_thumbnail_url();
                 } else {
                     echo get_template_directory_uri() . '/img/postcover.svg';
                 } ?>" alt="<?php the_title(); ?>">
        </div>
        <a href="<?php echo get_permalink(); ?>"
           class="text-lg line-clamp-1 hover:line-clamp-none my-4"><?php the_title(); ?></a>
    </div>
    <div class="flex flex-row w-full items-center">
        <img class="h-10 w-10 rounded-full mr-2"
             src="<?php echo get_avatar_url(get_the_author_meta('ID')); ?>" alt="user">
        <div class="w-full">
            <p class="text-md"><?php the_author(); ?></p>
            <p class="text-sm"><?php echo get_the_date("F d Y"); ?></p>
        </div>
    </div>
</div>