<section class="px-4 pt-20 pb-20 bg-disable/10 dark:bg-disable/30 mb-32">
    <div class="container mx-auto">
        <div class="flex justify-center lg:justify-between items-center mb-8">
            <p class="text-3xl text-dark dark:text-light font-semibold">Article Tazkia</p>
            <a href="<?php echo site_url() . '/article' ?>"
               class="bg-primary text-light rounded-md py-2 px-4 hidden lg:block">Artikel Lainnya</a>
        </div>
        <div class="flex flex-wrap flex-row justify-between">
            <?php
            $arg = array(
                'numberposts' => 4,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'article',
                'post_status' => 'publish',
                'suppress_filters' => true,
            );
            $latest_article = wp_get_recent_posts($arg, $output = ARRAY_A);
            ?>
            <div class="basis-full lg:basis-6/12 pr-4 mb-4">
                <?php if (!empty($latest_article)): ?>
                    <div class="aspect-video overflow-hidden rounded-lg relative shadow-lg shadow-disable/10">
                        <img class="object-cover w-full h-full"
                             src="<?php if (has_post_thumbnail($latest_article[0]["ID"])) {
                                 echo get_the_post_thumbnail_url($latest_article[0]["ID"],);
                             } else {
                                 echo get_template_directory_uri() . '/img/postcover.svg';
                             } ?>"
                             alt="<?php echo $latest_article[0]['post_title']; ?>">
                    </div>
                    <a href="<?php echo get_permalink($latest_article[0]["ID"]); ?>"
                       class="text-2xl text-dark dark:text-light font-semibold line-clamp-1 hover:line-clamp-none my-3"><?php echo $latest_article[0]['post_title']; ?></a>
                    <p class="line-clamp-2 text-dark/80 dark:text-light/80"><?php echo get_the_excerpt($latest_article[0]["ID"]); ?></p>
                <?php endif ?>
            </div>
            <div class="basis-full lg:basis-6/12">
                <?php foreach ($latest_article as $i => $article): if ($i < 1) continue; ?>
                    <div class="flex flex-row mb-4 bg-light/50 dark:bg-dark/50 hover:bg-disable/10 dark:hover:bg-disable/30 rounded-lg p-3 shadow-lg shadow-disable/10">
                        <div class="aspect-video overflow-hidden rounded-lg relative w-44">
                            <img class="object-cover w-full h-full"
                                 src="<?php if (has_post_thumbnail($article["ID"])) {
                                     echo get_the_post_thumbnail_url($article["ID"], 'thumbnail');
                                 } else {
                                     echo get_template_directory_uri() . '/img/postcover.svg';
                                 } ?>"
                                 alt="<?php echo $article['post_title']; ?>">
                        </div>
                        <div class="flex-1 pl-3">
                            <a href="<?php echo get_permalink($article["ID"]); ?>"
                               class="text-xl text-dark dark:text-light font-semibold line-clamp-1 hover:line-clamp-none mb-2"><?php echo $article['post_title']; ?></a>
                            <p class="line-clamp-2 text-dark/80 dark:text-light/80"><?php echo get_the_excerpt($article['ID']); ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="text-center lg:hidden">
                    <a href="<?php echo site_url() . '/article' ?>"
                       class="bg-primary text-light rounded-md py-2 px-4 inline-block">Artikel Lainnya</a>
                </div>
            </div>
        </div>
    </div>
</section>