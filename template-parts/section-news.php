<section class="px-4 my-20">
    <div class="container mx-auto">
        <div class="text-center mb-4 dark:text-light">
            <p class="text-2xl font-bold font-DM-sans">Berita Terkini</p>
            <p>Jangan Lewatkan Berita Menarik Seputar Institut Tazkia</p>
        </div>
        <div class="swiper" id="news">
            <div class="swiper-wrapper flex items-stretch pb-8 mb-3">
                <?php
                $arg = array(
                    'numberposts' => 4,
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'suppress_filters' => true,
                );
                $lates_post = wp_get_recent_posts($arg);
                foreach ($lates_post as $post) :
                    ?>
                    <div class="flex flex-col justify-between bg-light dark:bg-disable/25 hover:bg-disable/5 dark:hover:bg-disable/50 dark:text-light shadow-xl shadow-disable/10 p-4 rounded-lg swiper-slide">
                        <div>
                            <div class="aspect-video overflow-hidden rounded-lg relative">
                                <img class="object-cover"
                                     src="<?php if (has_post_thumbnail($post["ID"])) {
                                         echo get_the_post_thumbnail_url($post['ID']);
                                     } else {
                                         echo get_template_directory_uri() . '/img/postcover.svg';
                                     } ?>"
                                     alt="<?php echo $post['post_title']; ?>">
                            </div>
                            <a href="<?php echo get_permalink($post["ID"]); ?>"
                               class="text-lg line-clamp-1 hover:line-clamp-none my-4"><?php echo $post['post_title']; ?></a>
                        </div>
                        <div class="flex flex-row w-full items-center">
                            <img class="h-10 w-10 rounded-full mr-2"
                                 src="<?php echo get_avatar_url($post['post_date']) ?>" alt="user">
                            <div class="w-full">
                                <p class="text-md"><?php the_author_meta('display_name', $post['post_author']); ?></p>
                                <p class="text-sm"><?php $date = date_create($post['post_date']);
                                    echo date_format($date, "d F Y"); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <a href="<?php echo site_url() . '/news' ?>"
                   class="self-stretch bg-light dark:bg-disable/25 hover:bg-primary hover:text-light dark:hover:bg-primary flex flex-col justify-center items-center dark:text-light shadow-xl shadow-disable/10 p-4 rounded-lg swiper-slide h-auto group">
                    <svg class="w-10 h-10 stroke-primary group-hover:stroke-light dark:stroke-secondary" fill="none"
                         stroke="currentColor" viewBox="0 0 24 24"
                         xmlns="http://www.w3.org/2000/svg">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                              d="M19 20H5a2 2 0 01-2-2V6a2 2 0 012-2h10a2 2 0 012 2v1m2 13a2 2 0 01-2-2V7m2 13a2 2 0 002-2V9a2 2 0 00-2-2h-2m-4-3H9M7 16h6M7 8h6v4H7V8z"/>
                    </svg>
                    <p class="block text-lg">Lihat Berita lainnya</p>
                </a>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
