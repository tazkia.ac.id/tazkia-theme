<div class="mt-7 flex justify-center items-center">
    <?php the_posts_pagination(array(
        'before_page_number' => '<span class="text-dark dark:text-light px-3 py-2 rounded-md hover:bg-disable/10 dark:hover:bg-disable/30">',
        'after_page_number' => '</span>',
        'prev_text' => '<span class="bg-primary text-light px-3 py-2 rounded-md">Prev</span>',
        'next_text' => '<span class="bg-primary text-light px-3 py-2 rounded-md">Next</span>',
        'end_size' => 0,
        'mid_size' => 0
    )); ?>
</div>