<section class="px-4">
    <div class="container mx-auto my-20 md:my-36">
        <div class="flex flex-wrap flex-row justify-between items-center">
            <div class="xl:basis-3/12 lg:basis-4/12 md:basis-5/12 basis-full mb-8 text-center md:text-left">
                <p class="text-5xl text-dark dark:text-light font-semibold mb-3">Fakta Menarik
                    Tazkia</p>
                <p class="mb-10 lg:text-lg text-dark/80 dark:text-light/80">Institut Tazkia Telah Berdiri sejakd
                    tahun 2000, dan terus berkembang hingga saat ini.</p>
                <a class="bg-secondary text-light rounded-md py-2 px-4" href="https://spmb.tazkia.ac.id">Daftar
                    Kuliah</a>
            </div>
            <div class="lg:basis-7/12 md:basis-6/12 basis-full grid sm:grid-cols-2 lg:grid-cols-3 gap-4 text-light">
                <div class="bg-primary rounded-md p-5 shadow-lg">
                    <div class="p-3 bg-light/10 rounded-md w-11 h-11 mb-3">
                        <svg class="fill-secondary" xmlns="http://www.w3.org/2000/svg"
                             viewBox="0 0 512 512">
                            <path d="M352 96c0 53-43 96-96 96s-96-43-96-96s43-96 96-96s96 43 96 96zM240 248V512l-48.4-24.2c-20.9-10.4-43.5-17-66.8-19.3l-96-9.6C12.5 457.2 0 443.5 0 427V224c0-17.7 14.3-32 32-32H62.3c63.6 0 125.6 19.6 177.7 56zm32 264V248c52.1-36.4 114.1-56 177.7-56H480c17.7 0 32 14.3 32 32V427c0 16.4-12.5 30.2-28.8 31.8l-96 9.6c-23.2 2.3-45.9 8.9-66.8 19.3L272 512z"/>
                        </svg>
                    </div>
                    <p class="text-lg font-semibold mb-2">Dosen</p>
                    <p>Tazkia memiliki 70 dosen ahli dibidangnya.</p>
                </div>
                <div class="bg-primary rounded-md p-5 shadow-lg">
                    <div class="p-3 bg-light/10 rounded-md w-11 h-11 mb-3">
                        <svg class="fill-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                            <path d="M160 64c0-35.3 28.7-64 64-64H576c35.3 0 64 28.7 64 64V352c0 35.3-28.7 64-64 64H336.8c-11.8-25.5-29.9-47.5-52.4-64H384V320c0-17.7 14.3-32 32-32h64c17.7 0 32 14.3 32 32v32h64V64L224 64v49.1C205.2 102.2 183.3 96 160 96V64zm0 256c-53 0-96-43-96-96s43-96 96-96s96 43 96 96s-43 96-96 96zm-26.7 32h53.3C260.3 352 320 411.7 320 485.3c0 14.7-11.9 26.7-26.7 26.7H26.7C11.9 512 0 500.1 0 485.3C0 411.7 59.7 352 133.3 352z"/>
                        </svg>
                    </div>
                    <p class="text-lg font-semibold mb-2">Mahasiswa Aktif</p>
                    <p>Tazkia memiliki 2.092 mahasiswa aktif</p>
                </div>
                <div class="bg-primary rounded-md p-5 shadow-lg">
                    <div class="p-3 bg-light/10 rounded-md w-11 h-11 mb-3">
                        <svg class="fill-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                            <path d="M219.3 .5c3.1-.6 6.3-.6 9.4 0l200 40C439.9 42.7 448 52.6 448 64s-8.1 21.3-19.3 23.5L352 102.9V160c0 70.7-57.3 128-128 128s-128-57.3-128-128V102.9L48 93.3v65.1l15.7 78.4c.9 4.7-.3 9.6-3.3 13.3s-7.6 5.9-12.4 5.9H16c-4.8 0-9.3-2.1-12.4-5.9s-4.3-8.6-3.3-13.3L16 158.4V86.6C6.5 83.3 0 74.3 0 64C0 52.6 8.1 42.7 19.3 40.5l200-40zM129.1 323.2l83.2 88.4c6.3 6.7 17 6.7 23.3 0l83.2-88.4c73.7 14.9 129.1 80 129.1 158.1c0 17-13.8 30.7-30.7 30.7H30.7C13.8 512 0 498.2 0 481.3c0-78.1 55.5-143.2 129.1-158.1z"/>
                        </svg>
                    </div>
                    <p class="text-lg font-semibold mb-2">Alumni</p>
                    <p>Tazkia telah meluluskan 2.791 Alumni</p>
                </div>
                <div class="bg-primary rounded-md p-5 shadow-lg">
                    <div class="p-3 bg-light/10 rounded-md w-11 h-11 mb-3">
                        <svg class="fill-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                            <path d="M337.8 5.4C327-1.8 313-1.8 302.2 5.4l-139 92.7L37.6 126C15.6 130.9 0 150.3 0 172.8V464c0 26.5 21.5 48 48 48H320v0H256V416c0-35.3 28.7-64 64-64l.3 0h.5c3.4-37.7 18.7-72.1 42.2-99.1C350.2 260 335.6 264 320 264c-48.6 0-88-39.4-88-88s39.4-88 88-88s88 39.4 88 88c0 18.3-5.6 35.3-15.1 49.4c29-21 64.6-33.4 103.1-33.4c59.5 0 112.1 29.6 144 74.8V172.8c0-22.5-15.6-42-37.6-46.9L476.8 98.1 337.8 5.4zM96 192h32c8.8 0 16 7.2 16 16v64c0 8.8-7.2 16-16 16H96c-8.8 0-16-7.2-16-16V208c0-8.8 7.2-16 16-16zm0 128h32c8.8 0 16 7.2 16 16v64c0 8.8-7.2 16-16 16H96c-8.8 0-16-7.2-16-16V336c0-8.8 7.2-16 16-16zM320 128c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16s-7.2-16-16-16H336V144c0-8.8-7.2-16-16-16zM640 368c0-79.5-64.5-144-144-144s-144 64.5-144 144s64.5 144 144 144s144-64.5 144-144zm-99.3-43.3c6.2-6.2 16.4-6.2 22.6 0s6.2 16.4 0 22.6l-72 72c-6.2 6.2-16.4 6.2-22.6 0l-40-40c-6.2-6.2-6.2-16.4 0-22.6s16.4-6.2 22.6 0L480 385.4l60.7-60.7z"/>
                        </svg>
                    </div>
                    <p class="text-lg font-semibold mb-2">Tahun Pengalaman</p>
                    <p>Tazkia memiliki pengalaman selama <?php echo date('Y') - date('Y', 921101986) ?> tahun</p>
                </div>
                <div class="bg-primary rounded-md p-5 shadow-lg">
                    <div class="p-3 bg-light/10 rounded-md w-11 h-11 mb-3">
                        <svg class="fill-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                            <path d="M173.8 5.5c11-7.3 25.4-7.3 36.4 0L228 17.2c6 3.9 13 5.8 20.1 5.4l21.3-1.3c13.2-.8 25.6 6.4 31.5 18.2l9.6 19.1c3.2 6.4 8.4 11.5 14.7 14.7L344.5 83c11.8 5.9 19 18.3 18.2 31.5l-1.3 21.3c-.4 7.1 1.5 14.2 5.4 20.1l11.8 17.8c7.3 11 7.3 25.4 0 36.4L366.8 228c-3.9 6-5.8 13-5.4 20.1l1.3 21.3c.8 13.2-6.4 25.6-18.2 31.5l-19.1 9.6c-6.4 3.2-11.5 8.4-14.7 14.7L301 344.5c-5.9 11.8-18.3 19-31.5 18.2l-21.3-1.3c-7.1-.4-14.2 1.5-20.1 5.4l-17.8 11.8c-11 7.3-25.4 7.3-36.4 0L156 366.8c-6-3.9-13-5.8-20.1-5.4l-21.3 1.3c-13.2 .8-25.6-6.4-31.5-18.2l-9.6-19.1c-3.2-6.4-8.4-11.5-14.7-14.7L39.5 301c-11.8-5.9-19-18.3-18.2-31.5l1.3-21.3c.4-7.1-1.5-14.2-5.4-20.1L5.5 210.2c-7.3-11-7.3-25.4 0-36.4L17.2 156c3.9-6 5.8-13 5.4-20.1l-1.3-21.3c-.8-13.2 6.4-25.6 18.2-31.5l19.1-9.6C65 70.2 70.2 65 73.4 58.6L83 39.5c5.9-11.8 18.3-19 31.5-18.2l21.3 1.3c7.1 .4 14.2-1.5 20.1-5.4L173.8 5.5zM272 192c0-44.2-35.8-80-80-80s-80 35.8-80 80s35.8 80 80 80s80-35.8 80-80zM1.3 441.8L44.4 339.3c.2 .1 .3 .2 .4 .4l9.6 19.1c11.7 23.2 36 37.3 62 35.8l21.3-1.3c.2 0 .5 0 .7 .2l17.8 11.8c5.1 3.3 10.5 5.9 16.1 7.7l-37.6 89.3c-2.3 5.5-7.4 9.2-13.3 9.7s-11.6-2.2-14.8-7.2L74.4 455.5l-56.1 8.3c-5.7 .8-11.4-1.5-15-6s-4.3-10.7-2.1-16zm248 60.4L211.7 413c5.6-1.8 11-4.3 16.1-7.7l17.8-11.8c.2-.1 .4-.2 .7-.2l21.3 1.3c26 1.5 50.3-12.6 62-35.8l9.6-19.1c.1-.2 .2-.3 .4-.4l43.2 102.5c2.2 5.3 1.4 11.4-2.1 16s-9.3 6.9-15 6l-56.1-8.3-32.2 49.2c-3.2 5-8.9 7.7-14.8 7.2s-11-4.3-13.3-9.7z"/>
                        </svg>
                    </div>
                    <p class="text-lg font-semibold mb-2">Penghargaan</p>
                    <p>Tazkia telah mendapatkan 2 penghargaan global dunia</p>
                </div>
                <div class="bg-primary rounded-md p-5 shadow-lg">
                    <div class="p-3 bg-light/10 rounded-md w-11 h-11 mb-3">
                        <svg class="fill-secondary" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                            <path d="M320 32c-8.1 0-16.1 1.4-23.7 4.1L15.8 137.4C6.3 140.9 0 149.9 0 160s6.3 19.1 15.8 22.6l57.9 20.9C57.3 229.3 48 259.8 48 291.9v28.1c0 28.4-10.8 57.7-22.3 80.8c-6.5 13-13.9 25.8-22.5 37.6C0 442.7-.9 448.3 .9 453.4s6 8.9 11.2 10.2l64 16c4.2 1.1 8.7 .3 12.4-2s6.3-6.1 7.1-10.4c8.6-42.8 4.3-81.2-2.1-108.7C90.3 344.3 86 329.8 80 316.5V291.9c0-30.2 10.2-58.7 27.9-81.5c12.9-15.5 29.6-28 49.2-35.7l157-61.7c8.2-3.2 17.5 .8 20.7 9s-.8 17.5-9 20.7l-157 61.7c-12.4 4.9-23.3 12.4-32.2 21.6l159.6 57.6c7.6 2.7 15.6 4.1 23.7 4.1s16.1-1.4 23.7-4.1L624.2 182.6c9.5-3.4 15.8-12.5 15.8-22.6s-6.3-19.1-15.8-22.6L343.7 36.1C336.1 33.4 328.1 32 320 32zM128 408c0 35.3 86 72 192 72s192-36.7 192-72L496.7 262.6 354.5 314c-11.1 4-22.8 6-34.5 6s-23.5-2-34.5-6L143.3 262.6 128 408z"/>
                        </svg>
                    </div>
                    <p class="text-lg font-semibold mb-2">Beasiswa</p>
                    <p>Tazkia telah memberikan beasisiwa +- 100 Miliar</p>
                </div>
            </div>
        </div>
    </div>
</section>