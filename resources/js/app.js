import Collapse from "flowbite/src/components/collapse";
// navbar
const navbar = document.getElementById('navbar');

if (document.body.classList.contains('admin-bar')) {
    navbar.classList.add('sm:top-11', 'md:top-8');
}

function addNavbarShadow() {
    if (window.scrollY > 10) {
        navbar.classList.add("shadow-xl", "shadow-disable/5", "dark:shadow-disable/10");
    } else {
        navbar.classList.remove("shadow-xl", "shadow-disable/5", "dark:shadow-disable/10");
    }
}

window.addEventListener('scroll', addNavbarShadow)

// navbar menu
const path = window.location.href;
const navMenuLink = document.querySelectorAll("#navbar-menu a")
navMenuLink.forEach(function (e) {
    if (e.href === path) {
        e.classList.remove("text-dark/80", "dark:text-white/80");
        e.classList.add("text-primary", "dark:text-secondary");
    }
})

// toggle theme
const root = document.documentElement;
if (localStorage.getItem("tazkia-theme") === "dark" || (!('tazkia-theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
    root.classList.add("dark");
} else {
    root.classList.remove("dark");
}

function toggleTheme() {
    root.classList.toggle("dark");
    root.classList.contains("dark") ? localStorage.setItem('tazkia-theme', 'dark') : localStorage.setItem('tazkia-theme', 'light');
}

const toggleThemeButton = document.getElementById("toggle-theme");
toggleThemeButton.addEventListener("click", toggleTheme);

class dropDownMenu {
    constructor(targetEl, trigerEl) {
        this.targetEl = targetEl
        this.trigerEl = trigerEl
        this.init()
    }

    init() {
        this.trigerEl.addEventListener('click', () => {
            this.targetEl.classList.toggle('hidden');
        })
        document.addEventListener('click', (ev) => {
            let clickedEl = ev.target;
            if (clickedEl != this.trigerEl) {
                this.targetEl.classList.add('hidden');
            }
        })
    }
}

const dropdownMenuList = document.querySelectorAll('[data-menu-toggle]');
dropdownMenuList.forEach(function (trigerEl) {
    let targetEl = document.getElementById(trigerEl.getAttribute('data-menu-toggle'));
    new dropDownMenu(targetEl, trigerEl);
})

if (document.getElementById('news')) {
    const swiper = new Swiper("#news", {
        slidesPerView: 1,
        breakpoints: {
            600: {slidesPerView: 2,},
            1024: {slidesPerView: 3},
            1280: {slidesPerView: 4},
        },
        grabCursor: true,
        spaceBetween: 20,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
    });
}